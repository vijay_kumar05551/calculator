//
//  ViewController.m
//  Calculator
//
//  Created by Cli16 on 11/18/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
int method;
int selectNumber;
float runningTotal;

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *resultLabel;
@property (strong, nonatomic) IBOutlet UIView *acBtn;
@property (strong, nonatomic) IBOutlet UIButton *plusMinusBtn;
@property (strong, nonatomic) IBOutlet UIButton *modulsBtn;
@property (strong, nonatomic) IBOutlet UIButton *dividedBtn;
@property (strong, nonatomic) IBOutlet UIButton *sevenBtn;
@property (strong, nonatomic) IBOutlet UIButton *eightBtn;
@property (strong, nonatomic) IBOutlet UIButton *nineBtn;
@property (strong, nonatomic) IBOutlet UIButton *multiplyBtn;
@property (strong, nonatomic) IBOutlet UIButton *minusBtn;
@property (strong, nonatomic) IBOutlet UIButton *plusBtn;
@property (strong, nonatomic) IBOutlet UIButton *equalBtn;
@property (strong, nonatomic) IBOutlet UIButton *sixBtn;
@property (strong, nonatomic) IBOutlet UIButton *threeBtn;
@property (strong, nonatomic) IBOutlet UIButton *dotBtn;
@property (strong, nonatomic) IBOutlet UIButton *fiveBtn;
@property (strong, nonatomic) IBOutlet UIButton *twoBtn;
@property (strong, nonatomic) IBOutlet UIButton *zeroBtn;
@property (strong, nonatomic) IBOutlet UIButton *fourBtn;
@property (strong, nonatomic) IBOutlet UIButton *oneBtn;

@end

@implementation ViewController
@synthesize resultLabel;
@synthesize acBtn;
@synthesize oneBtn;
@synthesize twoBtn;
@synthesize threeBtn;
@synthesize fourBtn;
@synthesize fiveBtn;
@synthesize sixBtn;
@synthesize sevenBtn;
@synthesize eightBtn;
@synthesize nineBtn;
@synthesize zeroBtn;
@synthesize multiplyBtn;
@synthesize plusBtn;
@synthesize minusBtn;
@synthesize plusMinusBtn;
@synthesize modulsBtn;
@synthesize dotBtn;
@synthesize equalBtn;
@synthesize dividedBtn;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)oneBtnPressed:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber +1;
    resultLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    
}
- (IBAction)twoBtnPressed:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber +2;
    resultLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    
}
- (IBAction)threeBtnPressed:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber +3;
    resultLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    
}
- (IBAction)fourBtnPressed:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber +4;
    resultLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    
}
- (IBAction)fiveBtnPressed:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber +5;
    resultLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    
}
- (IBAction)sixBtnPressed:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber +6;
    resultLabel.text=[NSString stringWithFormat:@"%i",selectNumber];

}

- (IBAction)seveBtnPressed:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber +7;
    resultLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    
}
- (IBAction)eightBtnPressed:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber +8;
    resultLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    
}
- (IBAction)nineBtnPressed:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber +9;
    resultLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    
}
- (IBAction)zeroBtnPressed:(id)sender {
    selectNumber=selectNumber *10;
    selectNumber=selectNumber +0;
    resultLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    
}
- (IBAction)dotBtnPressed:(id)sender {
    selectNumber=selectNumber *1;
    selectNumber=selectNumber +0;
  /*  if (selectNumber) {
        <#statements#>
    } else {
        <#statements#>
    }
    resultLabel.text=[NSString stringWithFormat:@"%i",selectNumber]; */
    
   
    
}

- (IBAction)acBtnPressed:(id)sender {
    method=0;
    selectNumber=0;
    runningTotal=0;
    resultLabel.text=[NSString stringWithFormat:@"0"];
    
}
- (IBAction)plusMinusBtnPressed:(id)sender {
    
    
}
- (IBAction)modulsBtnPressed:(id)sender {
    if (runningTotal==0) {
        runningTotal=selectNumber;
    }
    else
    {
        switch (method) {
            case 1:
                runningTotal=runningTotal *selectNumber;
                break;
                case 2:
                runningTotal=runningTotal -selectNumber;
                break;
                case 3:
                runningTotal=runningTotal /selectNumber;
                break;
            case 4:
                runningTotal=runningTotal +selectNumber;
            default:
                break;
        }
    }
    method=1;
    selectNumber=0;
}
- (IBAction)dividedBtnPressed:(id)sender {
    if (runningTotal==0) {
        runningTotal=selectNumber;
    }
    else
    {
        switch (method) {
            case 1:
                runningTotal=runningTotal /selectNumber;
                break;
            case 2:
                runningTotal=runningTotal -selectNumber;
                break;
            case 3:
                runningTotal=runningTotal *selectNumber;
                break;
            case 4:
                runningTotal=runningTotal +selectNumber;
            default:
                break;
        }
    }
    method=2;
    selectNumber=0;
    
    
}
- (IBAction)multiplyBtnPressed:(id)sender {
    if (runningTotal==0) {
        runningTotal=selectNumber;
    }
    else
    {
        switch (method) {
            case 1:
                runningTotal=runningTotal /selectNumber;
                break;
            case 2:
                runningTotal=runningTotal -selectNumber;
                break;
            case 3:
                runningTotal=runningTotal *selectNumber;
                break;
            case 4:
                runningTotal=runningTotal +selectNumber;
            default:
                break;
        }
    }
    method=3;
    selectNumber=0;

    
}


- (IBAction)minusBtnPressed:(id)sender {
    if (runningTotal==0) {
        runningTotal=selectNumber;
    }
    else
    {
        switch (method) {
            case 1:
                runningTotal=runningTotal +selectNumber;
                break;
            case 2:
                runningTotal=runningTotal -selectNumber;
                break;
            case 3:
                runningTotal=runningTotal *selectNumber;
                break;
            case 4:
                runningTotal=runningTotal /selectNumber;
            default:
                break;
        }
    }
    method=4;
    selectNumber=0;

    
    
}
- (IBAction)plusBtnPressed:(id)sender {
    
    if (runningTotal==0) {
        runningTotal=selectNumber;
    }
    else
    {
        switch (method) {
            case 1:
                runningTotal=runningTotal /selectNumber;
                break;
            case 2:
                runningTotal=runningTotal -selectNumber;
                break;
            case 3:
                runningTotal=runningTotal *selectNumber;
                break;
            case 4:
                runningTotal=runningTotal +selectNumber;
            default:
                break;
        }
    }
    method=5;
    selectNumber=0;

}


- (IBAction)equalBtnPressed:(id)sender {
    if (runningTotal==0) {
        runningTotal=selectNumber;
    }
    else
    {
        switch (method) {
            case 1:
                runningTotal=runningTotal /selectNumber;
                break;
            case 2:
                runningTotal=runningTotal -selectNumber;
                break;
            case 3:
                runningTotal=runningTotal *selectNumber;
                break;
            case 4:
                runningTotal=runningTotal +selectNumber;
            default:
                break;
        }
    }
    method=2;
    selectNumber=0;

    method=0;
    selectNumber=0;
    resultLabel.text=[NSString stringWithFormat:@"%.2f",runningTotal];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
